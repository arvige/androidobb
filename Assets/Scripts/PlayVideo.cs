using UnityEngine;
using System.Collections;

public class PlayVideo : MonoBehaviour 
{
	void Awake()
	{
		//Screen.orientation = ScreenOrientation.Portrait;
	}
	
	void OnGUI () 
	{
        if (GUI.Button(new Rect(30, Screen.height - 180, 130, 50), "Play Sample_VP8 from OBB"))
			Handheld.PlayFullScreenMovie("sample_VP8.webm", Color.black, FullScreenMovieControlMode.CancelOnInput);

        if (GUI.Button(new Rect(30, Screen.height - 120, 130, 50), "Play univac from OBB"))
            Handheld.PlayFullScreenMovie("univac.webm", Color.black, FullScreenMovieControlMode.CancelOnInput);

        if (GUI.Button(new Rect(Screen.width - 90, Screen.height - 60, 60, 50), "Quit"))
            Application.Quit();
	}
}
