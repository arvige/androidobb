var download : WWW;
var url = "packed_resource.unity3d";
var resourcePath = "LoadScene";
var guiOffset = 20;
var assetBundle : AssetBundle;

function StartDownload () {
	// Use fancy logic to get the actual path
	if (Application.platform == RuntimePlatform.Android)
		download = new WWW ("jar:file://" + Application.dataPath + "!/assets/" + url);
	else if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
		download = new WWW ("file://" + Application.dataPath + "/StreamingAssets/" + url);
	
	yield download;
	
	assetBundle = download.assetBundle;
	
	Application.LoadLevelAdditive(resourcePath);
}

function OnGUI()
{
	GUILayout.Space(guiOffset);
	GUILayout.BeginHorizontal();
	if (download == null)
	{
		
		if (GUILayout.Button("\n Download " + url + "\n"))
			StartDownload();	
	}
	else
	{
		if (download.error == null)
		{
			var progress = parseInt(download.progress * 100);
			GUILayout.Label(progress + "%");	
			
			if (download.isDone && GUILayout.Button("\n Unload Resource \n")	)
			{
				download.Dispose();
				download = null;
				assetBundle.Unload (true);
				assetBundle = null;
			}
		}
		else
		{
			GUILayout.Label(download.error);		
		}	
	}
	
	GUILayout.EndHorizontal();
}